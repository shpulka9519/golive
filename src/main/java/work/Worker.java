package work;

public class Worker {
    protected String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String work() {
        String work = "Я просто працюю";
        return work;
    }

    public String work(int hour) {
        String work = "Я просто працюю " + hour + " годин.";
        return work;
    }

}
