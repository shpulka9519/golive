package work;

public class Developer extends Worker {
    public Developer(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name+"-dev";
    }

    @Override
    public String work() {
        String work = "Я пишу код";
        return work;
    }
}
